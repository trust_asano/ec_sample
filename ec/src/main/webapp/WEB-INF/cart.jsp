<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
	integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
	crossorigin="anonymous">
<title>カート</title>
</head>
<body>


	<!-- Navbar content -->
	<nav class="navbar navbar-dark bg-dark">
		<span class="navbar-brand mb-0 h1">カート詳細</span> <a href="Product"
			type="button" class="btn btn-primary btn-lg" style="float: right">商品一覧へ</a>
	</nav>



	

	<!--  
	<form method="POST" action="">
		<div class="container-fluid">
			<div class="row">
				<c:forEach items="${orders}" var="order">
					<div class="fixed-bottom" style="text-align: right">
						<div class="col-md-4 offset-md-7">
							<li>${order.getName()}</li>
							<li>${order.getQuantity()}</li> 個数:<input type="text" size="5"><br>


							<font size="4">合計 : (円)</font><br>

							<button type="submit" class="btn btn-primary btn-lg">注文する</button>
						</div>
					</div>
				</c:forEach>
			</div>
		</div>
	</form>
-->
	<div class="col-md-6">
		<div class="container-fluid pt-5">
			<div class="row">

				<c:forEach var="order" items="${orders}">

					<div class="col-md-6">
						<div class="card m-3" style="margin: 0 auto;">
							<img src="img/soccer.PNG" class="card-img-top" alt="...">
							<div class="card-body">
								<h5 class="card-title">
									<c:out value="${order.getName()}" />
								</h5>
								<p>
									<c:out value="${order.getPrice()}"></c:out>
									円
								</p>
								<p>
									<c:out value="${order.getQuantity()}"></c:out>
									個
								</p>
							</div>
						</div>
					</div>
				</c:forEach>
			</div>
		</div>
	</div>
	<h3>aaaa</h3>
</body>
</html>