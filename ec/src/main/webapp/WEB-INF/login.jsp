<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link href="css/bootstrap.min.css" rel="stylesheet">

<title>ログイン</title>
</head>
<body>
	<form method="POST" action="LoginServlet" class="form-signin" role="form">
		<h2 class="form-signin-heading">ログイン</h2>
		<label for="inputEmail" class="sr-only">名前</label> <input name="name"
			type="text" id="inputName" class="form-control" placeholder="名前"
			required autofocus /> <label for="inputPassword"
			class="sr-only">パスワード</label> <input type="password"
			id="inputPassword" class="form-control" placeholder="パスワード" required
			name="password" />
		
		<button class="btn btn-lg btn-primary btn-block" type="submit">
			Sign in</button>
	</form>
	</div>

	<script src="js/bootstrap.min.js"></script>
</body>
</html>