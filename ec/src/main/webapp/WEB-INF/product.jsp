<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link href="css/bootstrap.min.css" rel="stylesheet">
<title>商品ページ</title>
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<div class="container-fluid">
			<a class="navbar-brand" href="#">product</a>
			<button class="navbar-toggler" type="button"
				data-bs-toggle="collapse" data-bs-target="#navbarNav"
				aria-controls="navbarNav" aria-expanded="false"
				aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarNav">
				<ul class="navbar-nav">
					<li class="nav-item"><a class="nav-link" href="#">カテゴリ①</a></li>
					<li class="nav-item"><a class="nav-link" href="#">カテゴリ②</a></li>
					<li class="nav-item"><a class="nav-link" href="#">カテゴリ③</a></li>
				</ul>
				<a href="" class="btn btn-primary">アカウント情報</a> 
				<a href="Cart_Servlet" class="btn btn-primary">カート</a>

			</div>
		</div>
	</nav>

	<div class="container-fluid pt-5">
		<div class="row">
			<c:forEach var="t" items="${products}">
				<div class="col-md-3">
					<div class="card m-3" style="margin: 0 auto;">
						<img src="img/soccer.PNG" class="card-img-top" alt="...">
						<div class="card-body">
							<h5 class="card-title">
								<c:out value="${t.getName()}" />
							</h5>
							<a href="Detail?keyword=${t.getId()}">商品詳細</a>
						</div>
					</div>
				</div>
			</c:forEach>
		</div>
	</div>





	<script src="js/bootstrap.bundle.min.js"></script>
</body>
</html>