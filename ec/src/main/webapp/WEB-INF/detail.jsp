<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
	integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
	crossorigin="anonymous">
<title>商品詳細ページ</title>
</head>
<body>

	<nav class="navbar navbar-dark bg-dark">
		<span class="navbar-brand mb-0 h1">商品詳細</span>
		<button type="button" class="btn btn-primary btn-lg"
			style="float: right">カート</button>
	</nav>


	<div class="container-fluid">
		<div class="row">
				<div class="card col-md-5" style="width: 18rem;">
					<img src="img/soccer.PNG" class="card-img-top" alt="画像のサンプル"
						width="100%" height="100%">
				</div>

				<div class="d-flex flex-column justify-content-between  col-md-6">
					<div class="d-flex flex-column bd-highlight mb-3">
						<h2><c:out value="${product.getName()}"/></h2>
						商品の詳細、商品の詳細、商品の説明
					</div>

					<div class="d-flex align-items-end" style="text-align: right">
						<form action="Detail" method="POST">
							個数:<input type="text" size="3" name="quantity">
							<input type="hidden" name="name" value="${product.getName()}">
							<input type="hidden" name="price" value="${product.getPrice()}">
							<input type="hidden" name="id" value="${product.getId()}">
							
							<button class="btn btn-primary ml-5" type="submit">カートに追加</button>
						</form>
					</div>
				</div>
		</div>
	</div>


</body>
</html>