<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
<form class="w-25 mx-auto" action="SignUp" method="post">
           	<label for="username" class="sr-only"></label>
           	<input class="form-control" id="username"
           			type="text" name="name" placeholder="ユーザname" 
           			required autofocus/>
           	<label for="password" class="sr-only"></label>
           	<input class="form-control" id="password"
           			type="password" name="password" placeholder="パスワード" 
           			required/>
     
           	<input class="btn btn-outline-primary my-1" type="submit" value="Sign Up"/>
        </form>
        <script src="js/bootstrap.bundle.min.js"></script>
</body>
</html>