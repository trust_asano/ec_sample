<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
	integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
	crossorigin="anonymous">
<title>注文履歴</title>
</head>
<body>
	<p>
		<font size="6">過去の注文履歴</font>
	</p>
	<!-- 
	<div class="card">
		<a href="https://cajiya.co.jp/ec-demo/twbs-v5-base/products/detail/12">
			<img src="img/soccer.PNG " width="300" height="300">
		</a>
		<div class="card-body p-2 p-sm-3">
			<p class="card-title fs-6"><font size="5">商品の名前</font></p>
			<p class="card-text text-end"><font size="5">商品の値段</font></p>
		</div>
		<div class="card-footer text-end d-none d-sm-block">
			<a href="" class="btn btn-sm btn-outline-secondary">再度購入</a>
			商品詳細に飛ぶ
		</div>
	</div> 
	-->
	
	<div class="row border p-2 mb-2">
			<div class="col-4 col-sm-1 d-flex align-items-center justify-content-center order-2 order-sm-1 my-2">
				
			</div>
			<div class="col-12 col-sm-7 order-1 order-sm-2 mb-2 mt-2">
				<div class="row">
					<div class="col-3 col-sm-2">
						<img src="img/soccer.PNG"  class="img-fluid">
					</div>
					<div class="col-9 col-sm-10 small lh-sm">
						サッカーボール <br> 価格：￥3,000
					</div>
				</div>
			</div>
			<div
				class="col-4 col-sm-2 d-flex align-items-center justify-content-center order-3 my-2">
				<div>
					
					<div class="btn-group">
						<div><small>数量 : </small></div>
								1
					</div>
				</div>
			</div>
			<div
				class="col-4 col-sm-2 d-flex align-items-center justify-content-center order-4 my-2">
				￥3,000</div>
				
		</div>	
</body>
</html>