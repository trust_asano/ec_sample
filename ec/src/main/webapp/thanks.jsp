<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
<title>注文完了</title>
</head>
<body>
<form method="POST" action="History"><!-- postして過去の履歴全件表示 -->
<nav class="navbar navbar-dark bg-dark">
		<span class="navbar-brand mb-0 h1">Navbar</span>
		<button type="submit" class="btn btn-primary btn-lg"
			style="float: right">注文履歴</button>
	</nav>
</form>


 <p><font size="5">ご注文ありがとうございました！</font></p>
 <p>
          <a href="Product" class="btn btn-primary my-2">商品一覧ページへ</a>
          
        </p>
</body>
</html>