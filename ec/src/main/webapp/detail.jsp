<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
	integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
	crossorigin="anonymous">
<title>商品の詳細</title>
</head>
<body>

	<nav class="navbar navbar-dark bg-dark">
		<span class="navbar-brand mb-0 h1">Navbar</span>
		<button type="button" class="btn btn-primary btn-lg"
			style="float: right">カート</button>
	</nav>

<form method="POST" action="Detail">
	<div class="container-fluid">
		<div class="row">
			<div class="card col-md-4" style="width: 18rem;">
				<img src="img/soccer.PNG " width="300" height="300"><br>
				<div >
						<p>サッカーボール</p><p>価格：￥3,000</p>
					</div>
			</div>
			
			<div class="d-flex flex-column justify-content-between  col-md-4">
				<div class="d-flex flex-column bd-highlight mb-3">
				<h2>商品のタイトル</h2>
				商品の詳細、商品の詳細、商品の説明,商品の説明商品の説明商品の説明商品の説明商品の説明商品の説明
				</div>
				
				<!-- <div class="d-flex align-items-end" style="text-align: right">
					個数:<input type="text" size="3" name="number"> -->
					<div >
					数量 : <input type="number" id name="quantity" required="required" min="1" maxlength="9" class="form-control form-control" value="1" >
					</div><button class="btn btn-primary ml-5" type="submit">カートに追加</button>
					
				</div>
			
				
			</div>
		</div>
	</div>
</form>

</body>
</html>