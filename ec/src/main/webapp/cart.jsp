<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
	integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
	crossorigin="anonymous">
<title>カート</title>
</head>
<body>


	<!-- Navbar content -->
	<nav class="navbar navbar-dark bg-dark">
		<span class="navbar-brand mb-0 h1">Navbar</span>
		<button type="button" class="btn btn-primary btn-lg"
			style="float: right">カート</button>
	</nav>


<!-- 	<div class="card" style="width: 18rem;">
		<img src="img/soccer.PNG" class="card-img-top" alt="画像のサンプル"
			width="100%" height="100%">
		<div class="card-body">
			<p class="card-text">テキスト</p>

		</div>
	</div> -->



	<form method="POST" action="Thanks">
		<!-- 
		<div class="container-fluid">
			<div class="row"> -->
		
		<!-- 	
			</div>
		</div> -->

		<br>
		<br>
		<div class="row border p-2 mb-2">
			<div class="col-4 col-sm-1 d-flex align-items-center justify-content-center order-2 order-sm-1 my-2">
				
			</div>
			<div class="col-12 col-sm-7 order-1 order-sm-2 mb-2 mt-2">
				<div class="row">
					<div class="col-3 col-sm-2">
						<img src="img/soccer.PNG"  class="img-fluid">
					</div>
					<div class="col-9 col-sm-10 small lh-sm">
						サッカーボール <br> 価格：￥3,000
					</div>
				</div>
			</div>
			<div
				class="col-4 col-sm-2 d-flex align-items-center justify-content-center order-3 my-2">
				<div>
					
					<div class="btn-group">
						<div><small>数量</small></div>
					<input type="number" id name="quantity" required="required" min="1" maxlength="9" class="form-control form-control" value="1">
					</div>
				</div>
			</div>
			<div
				class="col-4 col-sm-2 d-flex align-items-center justify-content-center order-4 my-2">
				￥3,000</div>
		</div>
	
	
	
	<div  style="text-align: right">
			<!-- <div class="col-md-4 offset-md-7"> -->
			<div>
				点数 : 〇点<br> 
				<div class="fs-2 py-3">
                    <small class="fs-5">合計：</small>￥3,000
                  </div>
				
				<button type="submit" class="btn btn-primary btn-lg">注文する</button>
				
			</div>
		</div>
	</form>
</body>
</html>