package service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import beans.ProductBeans;
import beans.UserBeans;
import utils.Db;

public class EcService {
	public void SignUp(UserBeans ub) {
		String sql = "INSERT INTO user(name, password) values (?,?)";
		PreparedStatement stmt = null;
		try {
			Connection c = Db.open();
			stmt = c.prepareStatement(sql);
			stmt.setString(1, ub.getName());
			stmt.setString(2, ub.getPassword());
			
			
			stmt.executeUpdate();
			System.out.println("行を追加しました");
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	
	public UserBeans findUser(String name, String password) {
		// TODO 自動生成されたメソッド・スタブ
		String sql = "SELECT * FROM user WHERE name = ? AND password = ?";
		ResultSet rs = null;
		PreparedStatement stmt = null;
		UserBeans ub = null;
		try {
			Connection c = Db.open();
			stmt = c.prepareStatement(sql);
			stmt.setString(1, name);
			stmt.setString(2, password);
			rs = stmt.executeQuery();

			while (rs.next()) {
				ub = new UserBeans(
						rs.getInt("id"),
						rs.getString("name"),
						rs.getString("password")
						);
			} 
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return ub;
	}
	
	
	public ArrayList<ProductBeans> searchAll() {
		// TODO 自動生成されたメソッド・スタブ
		String sql = "SELECT * FROM product";
		ResultSet rs = null;
		PreparedStatement stmt = null;
		ArrayList<ProductBeans> products = new ArrayList<>();
		try {
			Connection c = Db.open();
			stmt = c.prepareStatement(sql);
			rs = stmt.executeQuery();

			while (rs.next()) {
				products.add(new ProductBeans(
						rs.getInt("id"),
						rs.getString("name"),
						rs.getInt("price"),
						rs.getInt("quantity")
						));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return products;
	}
	
	
	public ProductBeans searchById(int key) {
		// TODO 自動生成されたメソッド・スタブ
		String sql = "SELECT * FROM product WHERE id = ?";
		ResultSet rs = null;
		PreparedStatement stmt = null;
		ProductBeans product = null;
		try {
			Connection c = Db.open();
			stmt = c.prepareStatement(sql);
			stmt.setInt(1, key);
			rs = stmt.executeQuery();

			while (rs.next()) {
				product = new ProductBeans(
						rs.getInt("id"),
						rs.getString("name"),
						rs.getInt("price"),
						rs.getInt("quantity")
						);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return product;
	}
	
	
	
//	public ProductBeans SearchHistory(ProductBeans pb) {
//		String sql = "SELECT * FROM order_history WHERE user_id=?";
//		ResultSet rs = null;
//		PreparedStatement stmt = null;
//		ProductBeans products = null;
//		try {
//			Connection c = Db.open();
//			stmt = c.prepareStatement(sql);
//			rs = stmt.executeQuery();
//
//			while (rs.next()) {
//				products = new ProductBeans(
//						rs.getInt("id"),
//						rs.getInt("user_id"),
//						rs.getString("name"),
//						rs.getInt("price"),
//						rs.getInt("quantity"));
//			}
//		} catch (SQLException e) {
//			e.printStackTrace();
//		} finally {
//			try {
//				if (rs != null) {
//					rs.close();
//				}
//				if (stmt != null) {
//					stmt.close();
//				}
//			} catch (SQLException e) {
//				e.printStackTrace();
//			}
//		}
//		return products;
//	}
}
