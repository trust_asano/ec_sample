package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ProductBeans;
import service.EcService;
import utils.Db;

/**
 * Servlet implementation class Detail
 */
@WebServlet("/Detail")
public class Detail extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Detail() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		try {
			int keyword = Integer.parseInt(request.getParameter("keyword"));
			EcService es = new EcService();
			ProductBeans product = es.searchById(keyword);
			Db.close();
			request.setAttribute("product",product);
			
			
			getServletContext().getRequestDispatcher("/WEB-INF/detail.jsp").forward(request, response);
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		ArrayList<ProductBeans> orders = (ArrayList<ProductBeans>)session.getAttribute("orders");
		
		if(orders == null) {
			orders = new ArrayList<ProductBeans>();
		}
		
		String name = request.getParameter("name");
		int price = Integer.parseInt(request.getParameter("price"));
		int id = Integer.parseInt(request.getParameter("id"));
		int quantity = Integer.parseInt(request.getParameter("quantity"));
		
		orders.add(new ProductBeans(id, name, price, quantity));
		
		session.setAttribute("orders", orders);
		response.sendRedirect("Product");
	}

}
