package beans;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@AllArgsConstructor
@RequiredArgsConstructor
public class ProductBeans {
	private int id;
	private final String name;
	private final int price;
	private final int quantity;
}
