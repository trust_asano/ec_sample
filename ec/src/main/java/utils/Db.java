package utils;


import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;



public class Db {
	private static Connection con;

	public static Connection open() {
		try {
//			if (con == null) {
				Context initContext = new InitialContext();
				Context envContext = (Context)initContext.lookup("java:/comp/env");
				DataSource ds = (DataSource)envContext.lookup("jdbc/mariadb");
				con = ds.getConnection();
				System.out.println("接続成功");
	//		}
				
				
		} catch (Exception e) {
			e.printStackTrace();
		}
		return con;
	}

	public static void close() {
		if (con != null) {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	
}
